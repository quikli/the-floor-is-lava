===========================================================
The Floor is lava v1.0
===========================================================

Written for pyweek #19
    See http://www.pyweek.org

Author: Steven Wilson

Artwork: Steven Wilson

Sound: Steven Wilson

License: GPLv3

System Requirements:
    python 3.4 or python 2.7
    pygame 1.9
    resolution capabilities of 1920x1080 (1080p)

Known to be Unsupported:
    lower resolutions

    This game is written for python 3.4 or 2.7 and pygame 1.9
and has no other dependencies.

    Lower resolutions are tough to implement with the
infrastructure as it is.  The game is tuned for a 30x17
tile map and the tiles are 64x64.  Scaling could be used,
but would require excessive casting to int and
velocities/timings would need to be adjusted.

    Gamepad play (XBOX360 controller and 1 other tested to
work correctly with the game) is recommened, but not
required, keyboard controls are available.  The game may
be played by up to 4 players at a time (each on their own
input device, though the game will allow you to control
2 players with 1 input device... poorly).

===========================================================
Game Objective
===========================================================

    The game is simple.  You are trapped in a single room
and the Floor is crumbling over lava.  Collect as many
coins as you can before falling into lava.  Collect more
than your friends and put your name on the high score
board.

Good Luck!

===========================================================
CONTROLS: The game supports keyboard and joystick controls
===========================================================
    Keyboard controls are:
        w,a,s,d or arrow keys: movement and selection
        space bar: jump
        enter: confirm selection

    Joystick/gamepad controls are:
        Joystick axes 0&1: movement
        hat up and down: selection
        button 0: jump and confirm selection

to reset scores, run resetHighScores.py in the bin directory
