import enum
import time
from typing import Tuple, Dict, List

import numpy as np
import pygame


class ImageTag(enum.IntEnum):
    COIN_0 = 0
    COIN_1 = enum.auto()
    COIN_2 = enum.auto()
    COIN_3 = enum.auto()
    COIN_4 = enum.auto()
    COIN_5 = enum.auto()
    PLAYER_0 = enum.auto()
    PLAYER_1 = enum.auto()
    PLAYER_2 = enum.auto()
    PLAYER_3 = enum.auto()
    PLAYER_4 = enum.auto()
    FOUNDATION = enum.auto()
    LAVA = enum.auto()
    FLOOR = enum.auto()
    SPLASH = enum.auto()


class SoundTag(enum.IntEnum):
    BOUNCE = 0
    SPLASH = enum.auto()
    BLIP = enum.auto()


class FontTag(enum.IntEnum):
    ARCHERY_32 = 0
    ARCHERY_48 = enum.auto()
    ARCHERY_64 = enum.auto()


class Screen(enum.IntEnum):
    MENU = 0
    MATCH = enum.auto()
    SCORES = enum.auto()


class Color(enum.Enum):
    BLACK = pygame.Color(0, 0, 0)
    GREEN = pygame.Color(0, 255, 0)
    WHITE = pygame.Color(255, 255, 255)


class Match:
    height: int
    shape: Tuple[int, int]
    size: int
    width: int

    def __init__(self, width: int, height: int):
        self.height = height
        self.shape = (width, height)
        self.size = width * height
        self.width = width
        self.health = np.array(self.shape, dtype=np.uint16)
        self.alive = np.ones(self.shape, dtype=np.dtype("bool"))
        self.damage = np.random.randint(0, 5, self.size * 4, dtype=np.uint8)


class Menu:
    name: str
    options: List[str]
    selection_index: int

    def __init__(self, name, options):
        self.name = name
        self.options = options
        self.selection_index = 0

    def __iadd__(self, offset: int):
        self.selection_index = (self.selection_index + offset) % len(self.options)
        return self

    def __isub__(self, offset: int):
        self.selection_index = (self.selection_index - offset) % len(self.options)
        return self

    @property
    def selection(self):
        return self.options[self.selection_index]

    @classmethod
    def main_menu(cls):
        return cls(
            "The Floor is Lava",
            [
                "Play Game",
                "Exit Game",
            ],
        )


image_paths: Dict[ImageTag, str] = {
    ImageTag.COIN_0: "images/coin_0.png",
    ImageTag.COIN_1: "images/coin_1.png",
    ImageTag.COIN_2: "images/coin_2.png",
    ImageTag.COIN_3: "images/coin_3.png",
    ImageTag.COIN_4: "images/coin_4.png",
    ImageTag.COIN_5: "images/coin_5.png",
    ImageTag.PLAYER_0: "images/player_0.png",
    ImageTag.PLAYER_1: "images/player_1.png",
    ImageTag.PLAYER_2: "images/player_2.png",
    ImageTag.PLAYER_3: "images/player_3.png",
    ImageTag.PLAYER_4: "images/player_4.png",
    ImageTag.FOUNDATION: "images/foundation.png",
    ImageTag.LAVA: "images/lava.png",
    ImageTag.FLOOR: "images/floor.png",
    ImageTag.SPLASH: "images/splash.png",
}


sound_paths: Dict[SoundTag, str] = {
    SoundTag.BLIP: "sounds/blip.wav",
    SoundTag.BOUNCE: "sounds/bounce.wav",
    SoundTag.SPLASH: "sounds/splash.wav",
}


font_paths: Dict[FontTag, Tuple[str, int]] = {
    FontTag.ARCHERY_32: ("fonts/SF_Archery_Black_SC.ttf", 32),
    FontTag.ARCHERY_48: ("fonts/SF_Archery_Black_SC.ttf", 48),
    FontTag.ARCHERY_64: ("fonts/SF_Archery_Black_SC.ttf", 64),
}


def run() -> None:
    pygame.display.init()
    native_width: int
    native_height: int
    native_resolution: Tuple[int, int]
    native_resolution = pygame.display.get_desktop_sizes()[0]
    native_width, native_height = native_resolution
    window: pygame.Surface = pygame.display.set_mode(native_resolution)
    pygame.display.set_caption("The Floor is Lava")
    bounds: pygame.Rect = window.get_rect()
    clock: pygame.time.Clock = pygame.time.Clock()

    screen: Screen = Screen.MENU
    menu: Menu = Menu.main_menu()
    match: Match

    images: List[pygame.Surface] = [
        pygame.image.load(image_paths[tag]).convert_alpha() for tag in ImageTag
    ]
    sounds: List[pygame.mixer.Sound] = [
        pygame.mixer.Sound(sound_paths[tag]) for tag in SoundTag
    ]
    fonts: List[pygame.font.Font] = [
        pygame.font.Font(*font_paths[tag]) for tag in FontTag
    ]

    running: bool = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if screen == Screen.MENU:
                if event.type == pygame.KEYDOWN:
                    if event.key in (pygame.K_UP, pygame.K_w):
                        sounds[SoundTag.BLIP].play()
                        menu -= 1
                    elif event.key in (pygame.K_DOWN, pygame.K_s):
                        sounds[SoundTag.BLIP].play()
                        menu += 1
                    elif event.key == pygame.K_RETURN:
                        if menu.selection == "Exit Game":
                            running = False
                        elif menu.selection == "Play Game":
                            match = Match(30, 17)
                            screen = Screen.MATCH
            elif screen == Screen.MATCH:
                # TODO
                pass
            elif screen == Screen.SCORES:
                # TODO
                pass

        if screen == Screen.MENU:
            window.blits(
                [
                    (images[ImageTag.FOUNDATION], (x * 64, y * 64))
                    for x, y in [(x, y) for x in range(30) for y in range(17)]
                ]
            )
            window.blits(
                [
                    (images[ImageTag.LAVA], (x * 64, y * 64))
                    for x, y in [(x, y) for x in range(30) for y in range(17)]
                ]
            )
            window.blits(
                [
                    (images[ImageTag.FLOOR], (x * 64, y * 64))
                    for x, y in [(x, y) for x in range(30) for y in range(17)]
                ]
            )
        elif screen == Screen.MATCH:
            pass
        elif screen == Screen.SCORES:
            pass

        pygame.display.flip()
        clock.tick(16)


if __name__ == "__main__":
    pygame.init()
    pygame.mixer.pre_init(44100, -16, 1, 512)
    pygame.joystick.init()
    Match(30, 17)
    run()
    pygame.quit()


# TODO
# * Multiple game loops -> single game loop
