try:
    import cPickle as pickle
except ImportError:
    import pickle


def writeScores(scores):
    with open('../highscores', 'wb') as f:
        pickle.dump(scores, f, protocol=2)

scores = [
    (50, 'Quikli'),
    (40, 'Quikli'),
    (30, 'Quikli'),
    (25, 'Quikli'),
    (20, 'Quikli'),
    (15, 'Quikli'),
    (10, 'Quikli'),
    (5, 'Quikli'),
    (3, 'Quikli'),
    (2, 'Quikli')
]

writeScores(scores)
