import math


class Vector:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def __sub__(self, v):
        return Vector(self.x - v.x, self.y - v.y)

    def __mul__(self, f):
        return Vector(self.x*f, self.y*f)

    def __iadd__(self, v):
        self.x += v.x
        self.y += v.y
        return self

    def __isub__(self, v):
        self.x -= v.x
        self.y -= v.y
        return self

    def __imul__(self, f):
        self.x *= f
        self.y *= f
        return self

    @property
    def length(self):
        return math.sqrt(self.y**2+self.x**2)

    @property
    def angle(self):
        if self.length != 0.0:
            theta = math.asin(self.y/self.length)*180/math.pi
            if self.x > 0:
                return theta
            else:
                return 180-theta
        else:
            return 0.0

    def set_length(self, length):
        if self.length != 0.0:
            factor = length/self.length
            self *= factor

    def normalize(self):
        self.set_length(1.0)
