import math
import moderngl
import moderngl_window
import os
import pygame

from dataclasses import dataclass, field
from moderngl_window import geometry
from pyrr import matrix44
from typing import Tuple, Any


@dataclass
class Game:
    cube_geometry: Any
    cube_resolution: Tuple[int, int]
    cube_surface: Any
    cube_texture: Any
    texture_program: Any


class Window(moderngl_window.WindowConfig):
    def __init__(self, **kwargs) -> None:
        self.resource_dir: str = "resources"
        self.title: str = "This is a Test"
        self.window_size: Tuple[int, int] = 640, 480
        self.width: int = 640
        self.height: int = 480
        super().__init__(**kwargs)
        self.game: Game = initialize(self)

    def render(self, time, frametime):
        render(self.game, self.ctx, time, frametime)


def initialize(window: Window) -> Game:
    cube_geometry = geometry.cube(size=(2.0, 2.0, 2.0))
    cube_resolution = (16, 16)
    cube_surface = pygame.Surface(cube_resolution, flags=pygame.SRCALPHA)
    cube_texture = window.ctx.texture(cube_resolution, 4)
    cube_texture.filter = moderngl.NEAREST, moderngl.NEAREST
    cube_texture.swizzle = "BGRA"

    perspective_matrix = matrix44.create_perspective_projection(60, window.wnd.aspect_ratio, 1, 100, dtype="f4")
    identity_matrix = matrix44.create_identity(dtype="f4")
    texture_program = window.load_program("cube_simple_texture.glsl")
    texture_program["m_proj"].write(perspective_matrix)
    texture_program["m_model"].write(identity_matrix)

    return Game(
        cube_geometry=cube_geometry,
        cube_resolution=cube_resolution,
        cube_surface=cube_surface,
        cube_texture=cube_texture,
        texture_program=texture_program,
    )


def render(game: Game, ctx: Any, time: float, frametime: float) -> None:
    flags: int = moderngl.DEPTH_TEST | moderngl.CULL_FACE  # type: ignore
    ctx.enable_only(flags)
    game.cube_surface.fill((255, 255, 255))
    N = 8
    for i in range(N):
        time_offset = 6.28 / N * i
        color = (i * 50) % 255, (i * 100) % 255, (i * 20) % 255
        position = (
            math.sin(time + time_offset) * 5 + game.cube_resolution[0] / 2,
            math.cos(time + time_offset) * 5 + game.cube_resolution[1] / 2,
        )
        size = 0.1 * (math.sin(time) * 4 + 15)
        pygame.draw.circle(game.cube_surface, color, position, size)
    texture_data = game.cube_surface.get_view("1")
    game.cube_texture.write(texture_data)

    rotate = matrix44.create_from_eulers((time * 0.2, time * 0.3, time * 0.4), dtype="f4")
    translate = matrix44.create_from_translation((0, 0, -3.5), dtype="f4")
    camera = matrix44.multiply(rotate, translate)
    game.texture_program["m_camera"].write(camera)
    game.cube_texture.use()
    game.cube_geometry.render(game.texture_program)


if __name__ == "__main__":
    moderngl_window.run_window_config(Window, args=("--window", "pygame2"))

